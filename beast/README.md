# Notes

## Managing Alertmanager SealedSecret

Read the contents of the secret from cluser
```
kubectl get -n monitoring secrets alertmanager-main -o jsonpath='{.data.alertmanager\.yaml}' | base64 -d
```
Generate the SealedSecret
```
kubeseal <beast/alertmanager-secret.yaml -o yaml > beast/alertmanager-sealedsecret.yaml
```

