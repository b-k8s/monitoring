# prometheus et al

## Attempt no 1 - use the manifests from the `kube-prometheus` repo **PREFERRED**
Uses `git subtree` to maintain `base/prometheus`.

Add the sub-repo like this:
```
git remote add -f kube-prometheus git@github.com:prometheus-operator/kube-prometheus.git
git subtree add --prefix base/kube-prometheus kube-prometheus/master --squash

```
Update the sub-repo from the remote like this:
```
git fetch kube-promethus master
git subtree pull --prefix base/kube-prometheus kube-prometheus master --squash
```

Then `kustomize` from there.

## Attempt no 2 - `kustomize` official helm chart **ABANDONED**

Recipe for this adapted from
[here](https://blog.container-solutions.com/using-helm-and-kustomize-to-build-more-declarative-kubernetes-workloads)
- the idea being to leverage the substantial work put into the chart, but to
make it declarative rather than imperative, and git-ops friendly. The chart
seems much more full-featured, better out-of-the-box dashboards, and alerts,
sidecars for updating ConfigMaps (dashboards) declaratively, etc.

1. Fetch the chart and extract the source locally.
```
mkdir -p charts

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

helm fetch \
--untar \
--untardir charts \
prometheus-community/kube-prometheus-stack
```
2. Copy out and edit `values.yaml` to suite your needs. Generally, to maintain
   sanity, the preference is to make changes via `kustomize` so we try not to
   change _anything_ in the chart's `values.yaml`.
```
cp charts/kube-prometheus-stack/values.yaml .
```
One change I have done here is to disable the Grafana test framework by adding
the below snippet under the `grafana:` section.
```
grafana:
  testFramework:
    enabled: false
```
3. Template the yaml out from the chart into a kustomize base:
```
mkdir -p base-from-chart

helm template kube-prometheus \
--output-dir base-from-chart \
--namespace monitoring \
--values values.yaml \
charts/kube-prometheus-stack
```
Node that this will generate both a `charts` and a `templates` directory in the
output directory because this chart also ships dependent charts. We need to
apply both of these to the cluster.

4. Create kustomization over `base-from-chart`
```
(cat <<EOF
---
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
EOF
find base-from-chart -type f | \
   grep -v kustomization.yaml | \
   sed 's/^base-from-chart/- \./') > base-from-chart/kustomization.yaml
```
5. Create overlay `kustomization`s for:
  - namespace
  - ingress
  - PVCs
  - dashboards
